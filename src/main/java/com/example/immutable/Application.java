package com.example.immutable;

public class Application {

    public static void main(String[] args) {

        var person = new Person("Jack", 30);
        Greeter.greet("1", person); // Jack, 30
        var personChangedRef = ReferenceChanger.change(person, "John", 32);
        Greeter.greet("2", personChangedRef); // ?
        Greeter.greet("3", person); // ???
    }
}
