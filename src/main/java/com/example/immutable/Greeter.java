package com.example.immutable;

public class Greeter {

    public static void greet(String hint, Person person) {
        System.out.println("[" + hint + "] Hi, my name is " + person.name() + ". I am " + person.age() + " years old.");
    }
}
