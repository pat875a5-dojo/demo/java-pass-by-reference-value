package com.example.immutable;

public record Person(String name, int age) {}
