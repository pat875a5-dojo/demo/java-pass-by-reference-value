package com.example.mutable;

public class ObjectChanger {

    public static Person change(final Person person, String name, int age) {
        person.setName(name);
        person.setAge(age);
        return person;
    }
}
