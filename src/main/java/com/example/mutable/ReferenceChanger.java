package com.example.mutable;

public class ReferenceChanger {

    public static Person change(Person person, String name, int age) {
        person = new Person(name, age);
        return person;
    }
}
