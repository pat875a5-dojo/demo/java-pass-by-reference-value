package com.example.mutable;

public class Greeter {

    public static void greet(String hint, Person person) {
        System.out.println("[" + hint + "] Hi, my name is " + person.getName() + ". I am " + person.getAge() + " years old.");
    }
}
